class Game {
    constructor(canvas) {
        this.width = 800;
        this.height = 600;
        this.mouseX = this.width/2;
        this.mouseY = this.height/2;
        this.initCanvas(canvas);
        this.initMouseMove();
        this.pointManager = new PointManager();
        this.fruitManager = new FruitManager(this.width, this.height);
        this.blade = new Blade(this.width/2, this.height/2);
        this.interval = setInterval(() => {
            this.update();
            this.render();
        }, 1000 / 60);
    }
    update() {
        this.fruitManager.update(this.blade, this.pointManager);
        this.blade.move();
    }
    render() {
        const ctx = this.ctx;
        const canvas = this.canvas;
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.fillStyle = '#964B00';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        this.fruitManager.render(ctx);
        this.blade.render(ctx);
        this.pointManager.render(ctx);
    }
    initCanvas(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.canvas.width = this.width;
        this.canvas.height = this.height;
    }
    initMouseMove(){
        this.canvas.addEventListener('mousemove',e=>{
            //console.log(e);
            this.mouseX = e.offsetX;
            this.mouseY = e.offsetY;
            this.blade.setDirection(this.mouseX, this.mouseY);
        });
    }

}