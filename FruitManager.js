class FruitManager {
    static colors = ['red', 'lightblue', 'green', 'yellow', 'orange', 'pink'];
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.fruits = [];
    }
    update(blade, pointManager) {
        this.fruits = this.fruits.filter(fruit=>{
            fruit.move()
            if(this.collide(fruit,blade)){
                if(fruit.cut()){
                    pointManager.addPoints(50);
                };
            }
            return fruit.y < this.height + 100;
        });
        this.addFruit();
        //this.doFruitGravity();
    }
    collide(o1, o2) {
        return (o1.x < o2.x + o2.width && o1.x + o1.width > o2.x && o1.y < o2.y + o2.height && o1.y + o1.height > o2.y) && o1 !== o2;
    }
    getRandomColor() {
        return FruitManager.colors[Math.floor(Math.random() * FruitManager.colors.length)]
    }
    addFruit() {
        if (Math.random() > 0.01) return;
        console.log('Adding fruit...');
        const fruitSize = 40 + Math.floor((Math.random() * 15));
        const x = Math.floor((this.width - fruitSize) * Math.random())
        this.fruits.push(
            new Fruit(
                x,
                //400,
                this.height + fruitSize,
                fruitSize,
                fruitSize,
                x > this.width / 2,
                this.getRandomColor()
            )
        )
    }
    render(ctx) {
        this.fruits.forEach(fruit => {
            fruit.render(ctx);
        })
    }
}