class PointManager{
    constructor(){
        this.points = 0;
    }
    addPoints(points){
        this.points += points;
    }
    render(ctx){
        ctx.fillStyle = 'white';
        ctx.font = '20px Arial';
        ctx.fillText(this.points + ' Points', 20, 30);
    }
}