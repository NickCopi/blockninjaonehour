class Fruit {
    constructor(x, y, width, height, xVelocityDirection, color) {
        this.x = x;
        this.y = y;
        this.yVelocity = -15 + (-(Math.random() * 8));
        this.xVelocity = (Math.floor(Math.random() * 5) + 4) * (xVelocityDirection?-1:1);
        this.width = width;
        this.height = height;
        this.color = color;
        this.sliced = false;
    }
    cut() {
        if(this.sliced) return false;
        this.xVelocity = 0;
        this.yVelocity = 0;
        this.color = 'gray';
        this.sliced = true;
        return true;
    }
    move(){
        this.x += this.xVelocity;
        this.y += this.yVelocity;
        this.yVelocity += 0.4;
    }
    render(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

}