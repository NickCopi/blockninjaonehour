class Blade {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.targetX = x;
        this.targetY = y;
        this.angle = 0;
        this.width = 10;
        this.height = 10;
        this.speed = 20;
        this.direction = { x: 0, y: 0 };
    }
    setDirection(x, y) {
        this.targetX = x;
        this.targetY = y;
        x -= this.x;
        y -= this.y;
        let xPos = x > 0;
        let yPos = y > 0;
        let theta = Math.atan(Math.abs(y) / Math.abs(x));
        if (xPos && !yPos) theta = Math.PI / 2 * 3 + (Math.PI / 2 - theta);
        if (!xPos && !yPos) theta = Math.PI / 2 * 2 + (theta);
        if (!xPos && yPos) theta = Math.PI / 2 + (Math.PI / 2 - theta);
        this.direction.x = Math.cos(theta);
        this.direction.y = Math.sin(theta);
        this.angle = theta;
    }
    dist(x1,y1,x2,y2){
		return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
	}
    move() {
        if(this.dist(this.x, this.y, this.targetX, this.targetY) < this.speed) return;
        this.x += this.direction.x * this.speed;
        this.y += this.direction.y * this.speed;
    }

    render(ctx) {
        ctx.beginPath();
        ctx.fillStyle = 'black';
        ctx.arc(this.x, this.y, this.width / 2, 0, Math.PI * 2);
        ctx.fill();
    }
}